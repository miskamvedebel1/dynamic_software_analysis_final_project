"""
### DAG continuously listening to a Kafka topic for a specific message

This DAG will always run and asynchronously monitor a Kafka topic for a message 
which causes the funtion supplied to the `apply_function` parameter to return a value.
If a value is returned by the `apply_function`, the `event_triggered_function` is 
executed. Afterwards the task will go into a deferred state again. 
"""

from airflow.decorators import dag
from pendulum import datetime
from airflow.providers.apache.kafka.sensors.kafka import (
    AwaitMessageTriggerFunctionSensor,
)
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
import json
import uuid


KAFKA_TOPIC = "sysdig-events"


def listen_function(message):
    """Checks if the message received indicates a pet is in
    a mood listed in `pet_moods_needing_a_walk` when they received the last
    treat of a treat-series."""

    message_content = json.loads(message.value())
    print(f"Full message: {message_content}")

    return message_content

def event_triggered_function(event, **context):
    "Kicks off a downstream DAG with conf and waits for its completion."

    # use the TriggerDagRunOperator (TDRO) to kick off a downstream DAG
    print(event)

@dag(
    start_date=datetime(2023, 5, 28),
    schedule="@continuous",
    max_active_runs=1,
    catchup=False,
    render_template_as_native_obj=True,
)
def listen_to_the_stream():
    listen_for_mood = AwaitMessageTriggerFunctionSensor(
        task_id="listen_for_mood",
        kafka_config_id="kafka_listener",
        topics=[KAFKA_TOPIC],
        # the apply function will be used from within the triggerer, this is
        # why it needs to be a dot notation string
        apply_function="listen_to_the_stream.listen_function",
        poll_interval=5,
        poll_timeout=1,
        event_triggered_function=event_triggered_function,
    )


listen_to_the_stream()