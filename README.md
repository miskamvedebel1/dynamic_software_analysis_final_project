# dynamic_software_analysis_final_project
Repository for the final project - Maksim Lebedev

## ABSTRACT
To keep up with increasing complexity of modern software, dynamic software analysis tools should evolve as well. Recent advancement of data science, data engineering and machine learning techniques in its turn can be useful for dynamic software analysis. The main focus of this project is to research possible features to be used by machine learning algorithms and build a pipeline for system calls collection and data pre-processing. 
Various tools for system calls collection were tested, the pipeline for data streaming and processing was built using following open-source tools:
- Apache Kafka - for data streaming;
- Apache Airflow - for data processing;
- Python - programming language for data preprocessing;
- Sysdig - for system calls monitoring;
- Streamlit - for data visualisation;
As an example of possible usage, HDBSCAN clustering technique was used to cluster vectorised representation of log messages. To make all the modules of the pipeline working together, docker-compose is used.
Keywords: data gathering, ETL, streaming, system calls, data engineering

## ARCHITECTURE

![Architecture of the pipeline](architecture.png "Pipeline architecture")

## HOW TO START PIPELINE

1. clone git repo: `git clone`
2. to start the services for the pipeline - except Airflow
    ```
    cd project_final
    docker-compose up -d
    ```
3. to start airflow
    ```
    cd /project_final/airflow
    docker-compose up -d
4. to install sysid to host - linux kernel
    ```
    curl -s https://s3.amazonaws.com/download.draios.com/stable/install-sysdig | sudo bash
    ```
    more information: https://github.com/annulen/sysdig-wiki/blob/master/How-to-Install-Sysdig-for-Linux.md
5. to start listening syscall:
    ```
    ./attach_by_name.sh <name_of_the_service>
    ```
    
    In this project:
    ```
    ./attach_by_name.sh hotrod-linux
    ```
6. to check that syscall are in the topic -
    ```
    ./read_topic.sh sysdig-events
    ```

## WHERE TO CHECK SERVICES

- localhost: 8081 - testing app (Hotrod)
- localhost: 8080 - airflow web
- localhost: 8501 - streamlit testing app


