import streamlit as st
import pandas as pd
import numpy as np
import requests
from hdbscan import HDBSCAN
import plotly.express as px

#init model:

st.title('Embedding for logs')

TOKEN = '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'
model_id = "sentence-transformers/all-MiniLM-L6-v2"

api_url = f"https://api-inference.huggingface.co/pipeline/feature-extraction/{model_id}"
headers = {"Authorization": f"Bearer {TOKEN}"}

def query(texts):
    response = requests.post(api_url, headers=headers, json={"inputs": texts, "options":{"wait_for_model":True}})
    return response.json()

file_path = "../connect-input-file/sysdig-outcome.txt"
texts = [line.replace('\n', '') for line in open(file_path).readlines()]
clean_texts = set([' '.join(x.split(' ')[2:]) for x in texts])
L = len(clean_texts)
st.write(f"There are {len(texts)} logs available")
st.write(f"There are {L} unique logs available")
st.write("Please select number of unique logs for analysis")
limit = st.slider(label="Number of logs", min_value=100, max_value=min(1_000, len(clean_texts)), value=500)
analysis = list(clean_texts)[L-limit:]

output = query(analysis)
embeddings = pd.DataFrame(output)
hdb = HDBSCAN(min_cluster_size=10).fit(embeddings)
hdb_labels = hdb.labels_
embeddings.loc[:, 'label'] = hdb_labels
embeddings.loc[:, 'event'] = analysis
embeddings.loc[:, 'parsed_event'] = embeddings.event.apply(lambda s: s.split(' ')[4])
event_counts = embeddings.groupby('parsed_event').event.count().reset_index().rename({'event': 'counts'}, axis=1)

fig1 = px.scatter(embeddings, x=0, y=1, color='label', hover_data=['event'])
fig2 = px.bar(event_counts.sort_values(by="counts"), x="parsed_event", y="counts")

tab1, tab2 = st.tabs(["Log clusters", "Event counts"])
with tab1:
    st.plotly_chart(fig1, theme="streamlit", use_container_width=True)
with tab2:
    st.plotly_chart(fig2, theme="streamlit", use_container_width=True)

st.subheader('Outlier Analysis')

option = st.number_input('Insert a number', min_value=0.0, max_value=0.99, value=0.9)

threshold = pd.Series(hdb.outlier_scores_).quantile(option)
embeddings.loc[:, 'outlier_score'] = hdb.outlier_scores_
embeddings.loc[:, 'outlier'] = embeddings.outlier_score > threshold

fig_outlier = px.scatter(embeddings, x=0, y=1, color='outlier', hover_data=['event'])
st.plotly_chart(fig_outlier, theme="streamlit", use_container_width=True)